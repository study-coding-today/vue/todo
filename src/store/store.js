import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);  // vue를 전역에 사용하기 위해 정의  this.store로 접근할 수 있다.

export const store = new Vuex.Store({
  
})
